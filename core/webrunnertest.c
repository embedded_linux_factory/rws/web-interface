// SPDX-License-Identifier: GPL-2.0-only

#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

/* Test*/

#define PATTERN_NAVBAR			"{ \"request\" : \"getnavbarlist\" }"
#define PATTERN_CONTENT			"{ \"request\" : \"loadcontent\", \"page_name\" : \"Home\" }"
#define PATTERN_CONTENT_ERR		"{ \"request\" : \"loadcontent\" }"
#define PATTERN_JSONREQUEST		"{ \"request\" : \"jsonrequest\", \"page_name\" : \"Home\" }"
#define PATTERN_JSONREQUEST_ERR "{ \"request\" : \"jsonrequest\" }"
#define PATTERN_QUIT			"{ \"request\" : \"quit\" }"

char *pattern_list[] = {PATTERN_NAVBAR,
						PATTERN_CONTENT,
						PATTERN_CONTENT_ERR,
						PATTERN_JSONREQUEST,
						PATTERN_JSONREQUEST_ERR,
						PATTERN_QUIT,
						NULL};

int main(int argc, char **argv)
{
	int sock = 0;
	char *sockpath = NULL;
	int data_len = 0;

	if (argc != 2)
	{
		fprintf(stderr, "invalid arguments\n");
		return -1;
	}

	sockpath = argv[1];

	sock = socket(AF_UNIX, SOCK_STREAM, 0);
	struct sockaddr_un remote;

	remote.sun_family = AF_UNIX;
	strcpy(remote.sun_path, sockpath);

	printf("Client: Trying to connect... \n");
	if (connect(sock, (struct sockaddr *)&remote, sizeof(struct sockaddr_un)) == -1)
	{
		printf("Client: Error on connect call \n");
		return 1;
	}
	else
	{
		for (unsigned int i = 0; pattern_list[i] != NULL; i++)
		{
			if (send(sock, pattern_list[i], (strlen(pattern_list[i]) + 1) * sizeof(char), 0) == -1)
			{
				printf("Client: Error on send() call maybe due to disconnection\n");
				break;
			}
			else
			{
				char answer[4096];
				if ((data_len = recv(sock, answer, 4096, 0)) > 0)
				{
					printf("%s", answer);
				}
				printf("\n");
			}
		}
	}
	close(sock);
	return 0;
}