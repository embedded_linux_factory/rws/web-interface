// SPDX-License-Identifier: GPL-2.0-only

#include <string.h>

#include "pages.h"

#include "pages/example/example.h"
#include "pages/homepage/homepage.h"

page_t *pages[] = {&home_page, &example_page, NULL};

json_t *pages_get_content(const char *page_name)
{
	json_t *content = NULL;
	for (unsigned int i = 0; pages[i] != NULL; i++)
	{
		if (strncmp(page_name, pages[i]->id, strlen(page_name) + 1) == 0)
		{
			content = json_object();
			if (content)
			{
				if (pages[i]->html)
					json_object_set_new(content, "html", json_string(pages[i]->html));
				if (pages[i]->script)
					json_object_set_new(content, "script", json_string(pages[i]->script));
				if (pages[i]->init)
					json_object_set_new(content, "init", json_string(pages[i]->init));
				if (pages[i]->clean)
					json_object_set_new(content, "clean", json_string(pages[i]->clean));
			}
		}
	}
	return content;
}

json_t *pages_json_request(json_t *jrequest)
{
	json_t *jreply = NULL;
	const char *page_name = json_string_value(json_object_get(jrequest, "page_name"));
	if (page_name)
	{
		for (unsigned int i = 0; pages[i] != NULL; i++)
		{
			if (strncmp(page_name, pages[i]->id, strlen(page_name) + 1) == 0)
			{
				if (pages[i]->json_request_cb)
				{
					jreply = pages[i]->json_request_cb(jrequest);
				}
				else
				{
					jreply = json_pack("{s:s}", "error", "Page found but no callback registered");
				}
			}
		}
		if (!jreply)
		{
			jreply = json_pack("{s:s}", "error", "Page not registered");
		}
	}
	else
	{
		jreply = json_pack("{s:s}", "error", "Page name not found");
	}
	return jreply;
}