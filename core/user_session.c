// SPDX-License-Identifier: GPL-2.0-only

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/queue.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "user_session.h"

struct user_session_s
{
	char *username;
	struct sockaddr_un remote;
	int session_sock;
	LIST_ENTRY(user_session_s) entries;
};

LIST_HEAD(user_session_list_head, user_session_s);

static struct user_session_list_head user_session_list;

void user_session_init()
{
	LIST_INIT(&user_session_list);
}

static struct user_session_s *user_session_get(char *username)
{
	struct user_session_s *us_iter = NULL;
	LIST_FOREACH(us_iter, &user_session_list, entries)
	{
		if (strncmp(username, us_iter->username, strlen(username) + 1) == 0)
		{
			return us_iter;
		}
	}
	return NULL;
}

int user_session_exist(char *username)
{
	struct user_session_s *us = user_session_get(username);
	if (us)
	{
		return 1;
	}
	return 0;
}

int user_session_new(char *username)
{
	if (username)
	{
		struct user_session_s *us = NULL;

		// Check if the user session already exists
		us = user_session_get(username);
		if (us)
		{
			return 0;
		}

		// else create a new user session
		us = malloc(sizeof(struct user_session_s));
		if (us)
		{
			memset(us, 0, sizeof(struct user_session_s));
			us->username = strdup(username);
			us->session_sock = socket(AF_UNIX, SOCK_STREAM, 0);
			if (us->session_sock >= 0)
			{
				us->remote.sun_family = AF_UNIX;
				snprintf(us->remote.sun_path, 108 /*UNIX_PATH_MAX*/, "/tmp/webrunner_%s", username);
				for (unsigned int try = 0; try < 5; try++)
				{
					if (connect(us->session_sock, (struct sockaddr *)&us->remote, sizeof(struct sockaddr_un)) == 0)
					{
						int on = 1;
						if (ioctl(us->session_sock, FIONBIO, (char *)&on) >= 0)
						{
							LIST_INSERT_HEAD(&user_session_list, us, entries);
							return 0;
						}
					}
					usleep(500000);
				}
				close(us->session_sock);
			}
			free(us);
		}
	}
	return -1;
}

int user_session_get_socket(char *username)
{
	struct user_session_s *us = user_session_get(username);
	if (us)
	{
		return us->session_sock;
	}
	return -1;
}

int user_session_delete(char *username)
{
	struct user_session_s *us = user_session_get(username);
	if (us)
	{
		if (us->session_sock >= 0)
			close(us->session_sock);
		if (us->username)
			free(us->username);
		LIST_REMOVE(us, entries);
		free(us);
	}
	return -1;
}
