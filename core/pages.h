// SPDX-License-Identifier: GPL-2.0-only

#ifndef PAGES_H
#define PAGES_H

#include <jansson.h>

typedef struct
{
	const char *id;
	const char *html;
	const char *script;
	const char *init;
	const char *clean;
	json_t *(*json_request_cb)(json_t *request);

} page_t;

json_t *pages_get_content(const char *page_name);
json_t *pages_json_request(json_t *jrequest);

#endif
