// SPDX-License-Identifier: GPL-2.0-only

#include <errno.h>
#include <fcntl.h>
#include <jansson.h>
#include <pwd.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#include "pages.h"

static int quit = 0;

char *getuser(uid_t uid)
{
	struct passwd *pws;
	pws = getpwuid(uid);
	if (pws)
		return pws->pw_name;
	return NULL;
}

const char *json_get_char(json_t *node, char *key)
{
	if (node && key)
	{
		json_t *value = json_object_get(node, key);
		if (value)
		{
			return json_string_value(value);
		}
	}
	return NULL;
}

typedef struct
{
	char *username;
	json_t *jcfg;
	FILE *log;
	struct sockaddr_un local_addr;
} wr_t;
static wr_t wr;

int wr_loadconfig()
{
	json_error_t jerr;
	wr.jcfg = json_load_file("/etc/webapp/webrunner.conf", JSON_DECODE_ANY, &jerr);
	if (wr.jcfg)
	{
		const char *logfile = json_get_char(wr.jcfg, "logfile");
		if (logfile)
		{
			wr.log = fopen("/tmp/webrunner.log", "a+");
		}
		if (!wr.log)
			wr.log = stdout;

		wr.username = getuser(getuid());
		if (wr.username)
		{
			const char *usp = json_get_char(wr.jcfg, "unix_socket_pattern");
			if (!usp)
				usp = strdup("/tmp/webrunner_");

			wr.local_addr.sun_family = AF_UNIX;
			snprintf(wr.local_addr.sun_path, 108 /*UNIX_PATH_MAX*/, "%s%s", usp, wr.username);
			return 0;
			free(wr.username);
		}
		else
		{
			fprintf(wr.log, "Can't get username (%s)\n", strerror(errno));
		}
		json_decref(wr.jcfg);
	}
	else
	{
		fprintf(stderr, "Can't load json file due to error line %d\n", jerr.line);
	}
	return -1;
}

void wr_free()
{
	if (wr.username)
		free(wr.username);
	json_decref(wr.jcfg);
	if (wr.log && (wr.log != stdout))
		free(wr.log);
	memset(&wr, 0, sizeof(wr_t));
}

int main()
{
	int sock = 0;

	if (wr_loadconfig() != 0)
		return -1;

	fprintf(wr.log, "Webrunner starting for user %s\n", wr.username);

	sock = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sock < 0)
	{
		fprintf(wr.log, "Webrunner starting for user %s\n", wr.username);
	}
	else
	{
		int new_sock = 0;
		struct sockaddr_un remote;
		unsigned int sock_len;

		bind(sock, (struct sockaddr *)&wr.local_addr, sizeof(struct sockaddr_un));
		chmod(wr.local_addr.sun_path, S_IRWXU | S_IRWXG | S_IRWXO);
		listen(sock, 1);

		new_sock = accept(sock, (struct sockaddr *)&remote, &sock_len);
		if (new_sock >= 0)
		{
			int rlen = 0;
			char rawdata[4096]; // Todo : json request max size is maximum 4096 bytes. Too short ???
			while (!quit)
			{
				memset(rawdata, 0, 4096 * sizeof(char));
				rlen = recv(new_sock, rawdata, 4096, 0);
				if (rlen > 0)
				{
					json_t *jdata = NULL;
					json_error_t jerror;
					json_t *jreply = json_object();
					char *reply = NULL;

					fprintf(wr.log, "Command received: %d : %s \n", rlen, rawdata);
					jdata = json_loads(rawdata, JSON_DECODE_ANY, &jerror);
					if (jdata)
					{
						const char *request = json_get_char(jdata, "request");
						if (request)
						{
							if (strncmp(request, "quit", 5) == 0)
							{
								fprintf(wr.log, "Exit command received -> quitting \n");
								quit = 1;
								json_object_set_new(jreply, "reply", json_string("quit succeed"));
							}
							else if (strncmp(request, "getnavbarlist", 14) == 0)
							{
								json_t *navbar = json_object_get(wr.jcfg, "navbar");
								if (navbar)
								{
									json_object_update(jreply, navbar); // todo
								}
								else
								{
									json_object_set_new(jreply, "error",
														json_string("Error: fail to get navigation bar informations"));
								}
							}
							else if (strncmp(request, "loadcontent", 12) == 0)
							{
								const char *page_name = NULL;
								page_name = json_get_char(jdata, "page_name");
								if (page_name)
								{
									json_t *content = pages_get_content(page_name);
									if (content)
									{
										json_object_set_new(jreply, "content", content);
									}
									else
									{
										json_object_set_new(
											jreply, "content",
											json_pack("{s:s}", "html",
													  "<div> <label>Error : cannot get page content</label></div>"));
									}
								}
								else
								{
									json_object_set_new(jreply, "content",
														json_pack("{s:s,s:s}", "html",
																  "<div> <label>Error : Page not found</label></div>",
																  "error", "Error: Missing page name in request"));
								}
							}
							else if (strncmp(request, "pagejsonrequest", 16) == 0)
							{
								json_object_set_new(jreply, "jsonreply", pages_json_request(jdata));
							}
							else
							{
								json_object_set_new(jreply, "error", json_string("Unknown request"));
							}
						}
						else
						{
							json_object_set_new(jreply, "error", json_string("No request received"));
						}
					}

					if (!jreply)
					{
						reply = strdup("{\"error\" : \"Internal error\"}");
					}
					else
					{
						reply = json_dumps(jreply, JSON_COMPACT);
						json_decref(jreply);
					}
					if (reply)
					{
						if (send(new_sock, reply, (strlen(reply) + 1) * sizeof(char), 0) == -1)
						{
							fprintf(wr.log, "Error on send() call \n");
						}
						free(reply);
					}
					else
					{
						fprintf(wr.log, "critical error\n");
						quit = -1;
					}
				}
				else
				{
					fprintf(wr.log, "Error on recv() call %s\n", strerror(errno));
					quit = -1;
				}
			}
			close(new_sock);
		}
		else
		{
			fprintf(wr.log, "Error on accept() call \n");
		}
		close(sock);
		unlink(wr.local_addr.sun_path);
	}
	wr_free();

	return 0;
}
