// SPDX-License-Identifier: GPL-2.0-only

/**
 * Generate the navigation bar html code from a JSON
 * @param {*} data 
 */
function generate_menubar (data)
{
    let menulist = JSON.parse(data);
    var menu = `<div id="nav" class="sidenav">`
    for (const key in menulist)
    {
        if (typeof menulist[key] == 'string')
        {
            menu += `<button onclick="loadcontent('${menulist[key]}');">${key}</button>`
        }
        else
        {
            menu += `
            <button class="dropdown-btn">${key}<i class="fa fa-caret-down"></i></button>
            <div class="dropdown-container">
            `
            submenu = menulist[key];
            for (const subkey in submenu) {
                menu += `<button onclick="loadcontent('${submenu[subkey]}');">${subkey}</button>`;
            }
            menu += `</div>`
        }
    }
    menu += `
    </div>`
    sessionStorage.setItem('navbar', menu)
}

/**
 * This function update the UI at startup and after connection
 */
function updateMainUI() {
    try {
        var user = sessionStorage.getItem("user");
        if (user == null)
        {
            sessionStorage.setItem('currentpage', '');
            document.getElementById('loggedinname').innerHTML = 'Not logged';
	        document.getElementById('main').innerHTML = `
            <form id="UserCredentialsForm" action="/webapp" method="post">
                <label for="name">Enter your credentials:</label>
                <input type="text" id="username" name="username" placeholder="username"><br>
                <input type="password" id="password" name="password" placeholder="password">
                <br>
                <button id="LogIn" type="submit" value="LogIn">Log In</button>
            </form>
            <div id="logerror" style="margin-top: 40px"></div>`;
	        const loginform = document.getElementById("UserCredentialsForm");
            loginform.addEventListener("submit", login);
        }
        else
        {
            document.getElementById('loggedinname').innerHTML = 'Logged as ' + user;
            document.getElementById('main').innerHTML = sessionStorage.getItem('navbar') + `
            <div id="content" style="overflow-y: scroll;">
                <label for="name">Everything is awesome</label>
            </div> `;
            // Set dropdown button action
            var dropdown = document.getElementsByClassName("dropdown-btn");
            var i;
            for (i = 0; i < dropdown.length; i++) {
                dropdown[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    var dropdownContent = this.nextElementSibling;
                    if (dropdownContent.style.display === "block") {
                        dropdownContent.style.display = "none";
                    } else {
                        dropdownContent.style.display = "block";
                    }
                });
            }
            // if we relaod the page, the current page has to be reloaded
            if (sessionStorage.getItem('currentpage') == '')
            {
                loadcontent('homepage')
            }
        }
    } catch (error) {
        console.error('Error fetching data from CGI script:', error);
    }
}

/**
 * This function is called to login to a specific user
 * @param {} event 
 */
async function login(event) {
    event.preventDefault();
    const form = event.target;
    let formData = "submitter_type=" + event.submitter.value + "&";

    formData = formData + new URLSearchParams((new FormData(form))).toString();
    try {
        const response = await fetch(form.action, {
            method: 'POST',
            body: formData ,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            }
        });

        const responseData = await response.text();
        const obj = JSON.parse(responseData);

        if (obj.user)
        {
            sessionStorage.setItem("user", obj.user);

            const navbarlist = await fetch("/webapp", {
                    method: 'POST',
                    body: "submitter_type=getnavbarlist&username="+obj.user ,
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    }
                });
            const answer = await navbarlist.text();
            generate_menubar(answer)
            updateMainUI();
        }
        else
        {
	        document.getElementById("logerror").innerText = obj.error;
        }
    } catch (error) {
        console.error('Error submitting form:', error);
    }
}

/**
 * This function is called to logout the current user session
 * @param {} event 
 */
async function logout() 
{
    var user = sessionStorage.getItem("user");
    try {
        const response = await fetch("/webapp", {
            method: 'POST',
            body: "submitter_type=LogOut&username=" + user,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            }
        });

        const responseData = await response.text();
        console.log('logout :', responseData)
        document.getElementById('main').innerHTML = responseData;
        sessionStorage.removeItem("user");
        sessionStorage.removeItem('currentpage');
        sessionStorage.removeItem('page_clean_cb');
        menulist = null;
        page_clean();
        updateMainUI();
    } catch (error) {
        console.error('Error submitting button data:', error);
    }
};

/**
 * This function call the current page function to clean its specific behaviour
 */
function page_clean()
{
    var page_clean_cb = sessionStorage.getItem('page_clean_cb');
    if (page_clean_cb)
    {
        eval(`${page_clean_cb}()`);
    }
    sessionStorage.removeItem('page_clean_cb')
}

/*
 * Call it to load the content of the select page
 */
async function loadcontent(Id) 
{
    var user = sessionStorage.getItem("user");
    page_clean();
    try {
        const response = await fetch('/webapp', {
            method: 'POST',
            body: "submitter_type=loadcontent&page_name=" + Id + "&username=" + user,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            }
        });

        const responseData = await response.text();
        console.log("loadcontent:", responseData)
        const obj = JSON.parse(responseData);

        var s = document.getElementById('page_script');
        if (s && s.parentNode)
            s.parentNode.removeChild(s);
        if (obj.content)
        {
            if (obj.content.script)
            {
                s = document.createElement( 'script' );
                s.id = 'page_script';
                s.type = 'text/javascript';
                s.src = 'pages/' + obj.content.script;
                s.innerHTML = null;
                document.body.appendChild( s );
            }
            if (obj.content.init)
            {
                s.addEventListener("load", () => {
                    eval(`${obj.content.init}()`);
                });
            }
            if (obj.content.clean)
            {
                sessionStorage.setItem('page_clean_cb', obj.content.clean);
            }
            document.getElementById('content').innerHTML = obj.content.html;
        }
        else if (obj.error)
        {
            document.getElementById('content').innerHTML = "<div>Error: " + obj.error + "</div>";
        }
        else
        {
            document.getElementById('content').innerHTML = "<div>Error: unhandled message from webrunner</div>";
        }

        sessionStorage.setItem('currentpage', Id);
    } catch (error) {
        console.error('Error submitting button data:', error);
    }
    return;
};

/*
 * Call it to send a request to the current page handler
 */
async function pagejsonrequest(jsonrequest) 
{
    var user = sessionStorage.getItem("user");
    jsonrequest.username = user;
    jsonrequest.request = "pagejsonrequest"
    jsonrequest.page_name = sessionStorage.getItem('currentpage');

    try {
        const response = await fetch('/webapp', {
            method: 'POST',
            body: JSON.stringify(jsonrequest),
            headers: {
                "Content-Type": "application/json",
            }
        });

        const responseData = await response.text();
        console.log("json reply:", responseData);
        return responseData;
    } catch (error) {
        console.error('Error requesting json from page:', error);
    }
    return;
};

window.addEventListener('load', updateMainUI);
