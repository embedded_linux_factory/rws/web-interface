// SPDX-License-Identifier: GPL-2.0-only

#ifndef USER_SESSION_H
#define USER_SESSION_H

void user_session_init();
int user_session_exist(char *username);
int user_session_new(char *username);
int user_session_get_socket(char *username);
int user_session_delete(char *username);

#endif