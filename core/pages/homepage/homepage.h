// SPDX-License-Identifier: GPL-2.0-only

#ifndef HOMEPAGE_H
#define HOMEPAGE_H

#include <pages.h>

extern page_t home_page;

#endif