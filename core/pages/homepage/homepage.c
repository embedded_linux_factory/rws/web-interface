// SPDX-License-Identifier: GPL-2.0-only

#include "homepage.h"

static const char html[] = " \
    <div id=\"homepage\">\
        <h2>Welcome to RWS</h2>\
    </div>\
";

page_t home_page = {
	.id = "homepage",
	.html = html,
	.script = NULL,
	.init = NULL,
	.clean = NULL,
	.json_request_cb = NULL,
};
