// SPDX-License-Identifier: GPL-2.0-only

var example_periodic_routine = null;

function example_init()
{
    console.log('home page init');
    example_periodic_routine = setInterval(example_periodic, 5000);
}

function example_clean()
{
    clearInterval(example_periodic_routine);
    console.log('home page clean');
}

function example_testbutton()
{
    console.log('home page test button');
    jreq = {"exammpl":"something"};
    let jreply = pagejsonrequest(jreq);
    console.log('home page json reply: ', jreply);
}

function example_periodic()
{
    console.log('home page periodic');
}