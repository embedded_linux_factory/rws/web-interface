// SPDX-License-Identifier: GPL-2.0-only

#include "example.h"

static const char html[] = " \
    <div id=\"example\">\
        <h2>Welcome:</h2>\
        <button type=\"button\" onclick=\"example_testbutton()\">Test button</button>\
    </div>\
";

json_t *example_json_request_cb(json_t *jrequest)
{
	json_t *jreply = NULL;
	if (jrequest)
	{
		jreply = json_pack("{s:s}", "example", "This is an example");
	}
	return jreply;
}

page_t example_page = {
	.id = "example",
	.html = html,
	.script = "example.js",
	.init = "example_init",
	.clean = "example_clean",
	.json_request_cb = example_json_request_cb,
};
