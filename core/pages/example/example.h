// SPDX-License-Identifier: GPL-2.0-only

#ifndef EXAMPLE_H
#define EXAMPLE_H

#include <pages.h>

extern page_t example_page;

#endif