// SPDX-License-Identifier: GPL-2.0-only

#include <crypt.h>
#include <errno.h>
#include <jansson.h>
#include <pwd.h>
#include <shadow.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

void usage()
{
	printf("websession uid gid executable\n");
}

static struct spwd *check_user(const char *username)
{
	if (username && (username[0] != 0))
	{
		struct spwd *user_shadow = getspnam(username);
		if (user_shadow)
		{
			return user_shadow;
		}
	}
	return NULL;
}

int check_password(struct spwd *shadow, const char *password, int allow_empty)
{
	if (password && shadow)
	{
		char *encrypted_password = NULL;
		if (password[0] == 0)
		{
			if (!allow_empty)
			{
				return -1;
			}
			else
			{
				printf("Warning: Empty password must be prohibited for security purposes\n");
				return 0;
			}
		}

		encrypted_password = crypt(password, shadow->sp_pwdp);
		if (strcmp(encrypted_password, shadow->sp_pwdp) == 0)
		{
			return 0;
		}
	}
	return -1;
}

static int switch_user(const char *username)
{
	struct passwd *pwd = getpwnam(username);
	if ((setgid(pwd->pw_gid) == 0) && (setuid(pwd->pw_uid) == 0))
	{
		printf("uid is %d gid is %d\n", getuid(), getgid());
		return 0;
	}
	return -1;
}

int main(int argc, char **argv)
{
	char *username = NULL, *password = NULL, *execname = NULL;
	struct spwd *shadow = NULL;
	pid_t pid = 0;

	if (argc != 4)
	{
		usage();
		return -1;
	}

	username = argv[1];
	password = argv[2];
	execname = argv[3];

	shadow = check_user(username);
	if (!shadow)
	{
		return -1;
	}

	if (check_password(shadow, password, 1) < 0)
	{
		return -1;
	}

	if (switch_user(username) == 0)
	{
		pid = fork();
		if (pid == -1)
		{
			return -1;
		}
		else if (pid > 0)
		{
			return pid;
		}
		else
		{
			execl(execname, execname, NULL);
			return -1;
		}
	}
	return -1;
}