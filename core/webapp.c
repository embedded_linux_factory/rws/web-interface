// SPDX-License-Identifier: GPL-2.0-only

// #define CLIDEBUGMODE 1

#include <crypt.h>
#include <errno.h>
#include <grp.h>
#ifndef CLIDEBUGMODE
#include <fcgi_config.h>
#include <fcgi_stdio.h>
#endif
#include <jansson.h>
#include <pwd.h>
#include <shadow.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <unistd.h>

#include "user_session.h"

#define CREDENTIALS_MAX_SIZE 64

typedef struct
{
	int len;
	char *command;
	char *action;
	char *username;
	char *password;
	char *page_name;
	json_t *json;
} post_cmd_t;

/**
 * \brief Handle the users connections
 * it will extract from the received_command the username and password to compute the system command and send it
 *
 * \param pc POST command structure
 * \return int -1 if error, 0 otherwise
 */
int handle_login(post_cmd_t *pc)
{
	int RetValue = 0; // return value of the function
	json_t *root = NULL;
	char *answer = NULL;

	root = json_object();
	if (root)
	{
		if (pc->username)
		{
			if (user_session_exist(pc->username))
			{
				json_object_set_new(root, "error",
									json_sprintf("%s is already logged. Please disconnect first", pc->username));
			}
			else
			{
				char cmd[1024];
				snprintf(cmd, 1024, "%s %s \"%s\" %s", "/usr/local/bin/websession", pc->username,
						 pc->password ? pc->password : "", "/usr/local/bin/webrunner");
				if (system(cmd) != -1)
				{
					if (user_session_new(pc->username) == 0)
					{
						// json_object_set_new( root, "cmd", json_string(cmd) );
						json_object_set_new(root, "user", json_string(pc->username));
					}
					else
					{
						json_object_set_new(root, "error", json_sprintf("user_session_new fail : %s", strerror(errno)));
					}
				}
				else
				{
					json_object_set_new(root, "error", json_sprintf("Fail to launch webrunner: %s", strerror(errno)));
				}
			}
		}
		else
		{
			json_object_set_new(root, "error", json_sprintf("Internal memory error: %s", strerror(errno)));
			RetValue = -1;
		}
		answer = json_dumps(root, JSON_COMPACT);
		json_decref(root);
		if (answer)
		{
			printf("%s\n", answer);
			free(answer);
		}
	}
	return RetValue;
}

static void json_data_send_and_print(json_t *jsondata, int sock)
{
	if (jsondata)
	{
		char *data = json_dumps(jsondata, JSON_COMPACT);
		if (data)
		{
			if (send(sock, data, (strlen(data) + 1) * sizeof(char), 0) == -1)
			{
				printf("{ \"error\" : \"Client: Error on send() call maybe due to disconnection\" }");
			}
			else
			{
				struct timeval timeout;
				fd_set working_set;

				FD_ZERO(&working_set);
				FD_SET(sock, &working_set);

				timeout.tv_sec = 0;
				timeout.tv_usec = 1000;

				if (select(sock + 1, &working_set, NULL, NULL, &timeout) > 0)
				{
					char answer[4096];
					int datalen;
					answer[4095] = 0;
					while ((datalen = recv(sock, answer, 4095, 0)) > 0)
					{
						printf("%s", answer);
					}
					printf("\n");
				}
			}
			free(data);
		}
		else
		{
			printf("{ \"error\" : \"json_dumps fail \" }");
		}
	}
	else
	{
		printf("{ \"error\" : \"invalid json file \" }");
	}
}

#ifdef CLIDEBUGMODE
#define POST_PATTERN_LIST_MEMBER (7)
char *pattern_list[POST_PATTERN_LIST_MEMBER] = {
	"submitter_type=LogIn&username=fabien&password=",
	"submitter_type=getnavbarlist&username=fabien",
	"submitter_type=loadcontent&username=fabien&page_name=Home",
	"{ \"username\" : \"fabien\", \"page_name\" : \"homepage\", \"request\" : \"pagejsonrequest\"}",
	"submitter_type=LogOut&username=fabien",
	"submitter_type=quit&username=fabien",
	NULL};

char *post_cmd_get_input_data(int *retsz)
{
	static int i = 0;
	char *str = pattern_list[i++];

	if (str)
	{
		*retsz = strlen(str);
		printf("Testing for %s\n", str);
		return str;
	}
	else
	{
		exit(0);
	}
}
#else
char *post_cmd_get_input_data(int *retsz)
{
	char *contentLength = getenv("CONTENT_LENGTH");
	int len = contentLength ? strtol(contentLength, NULL, 10) : 0;

	if (len > 0)
	{
		char *input = malloc(len + 1);
		if (input)
		{
			int i;

			for (i = 0; i < len; i++)
			{
				int ch;
				if ((ch = getchar()) < 0)
				{
					break;
				}
				input[i] = ch;
			}
			input[i] = '\0';
			if (i != len - 1)
			{
				*retsz = len;
				return input;
			}
			else
			{
				printf("[%d] %s\n", i, input);
				free(input);
			}
		}
	}
	return NULL;
}
#endif

static char *post_cmd_key_value(const char *txt, const char *key)
{
	char *value = NULL;

	if (txt && key)
	{
		char *start = strstr(txt, key);
		if (start)
		{
			char *middle = strchr(start, '=');
			if (middle)
			{
				char *end = strchrnul(middle + 1, '&');
				int sz = end - (middle + 1);
				if (sz > 0)
				{
					value = malloc(sz + 1);
					memcpy(value, middle + 1, sz);
					value[sz] = 0;
				}
			}
		}
	}
	return value;
}

post_cmd_t *post_cmd_parse()
{
	post_cmd_t *post_cmd = malloc(sizeof(post_cmd_t));
	if (post_cmd)
	{
		memset(post_cmd, 0, sizeof(post_cmd_t));
		post_cmd->command = post_cmd_get_input_data(&post_cmd->len);
		if (post_cmd->command)
		{
			if (post_cmd->command[0] == '{')
			{
				json_error_t jerror;
				post_cmd->json = json_loads(post_cmd->command, JSON_DECODE_ANY, &jerror);
				if (post_cmd->json)
				{
					post_cmd->username = strdup(json_string_value(json_object_get(post_cmd->json, "username")));
					if (post_cmd->username)
					{
						return post_cmd;
					}
					json_decref(post_cmd->json);
				}
			}
			else
			{
				post_cmd->action = post_cmd_key_value(post_cmd->command, "submitter_type");
				if (post_cmd->action)
				{
					post_cmd->username = post_cmd_key_value(post_cmd->command, "username");
					if (post_cmd->username)
					{
						post_cmd->password = post_cmd_key_value(post_cmd->command, "password");
						post_cmd->page_name = post_cmd_key_value(post_cmd->command, "page_name");
						return post_cmd;
					}
					free(post_cmd->action);
				}
			}
			free(post_cmd->command);
		}
		free(post_cmd);
	}
	else
	{
		printf("Fail to allocate memory\n");
	}
	return NULL;
}

void post_cmd_free(post_cmd_t *post_cmd)
{
	if (post_cmd)
	{
		if (post_cmd->action)
			free(post_cmd->action);
		if (post_cmd->username)
			free(post_cmd->username);
		if (post_cmd->page_name)
			free(post_cmd->page_name);
		free(post_cmd);
	}
}

int main()
{
#ifdef CLIDEBUGMODE
	for (unsigned int cd = 0; cd < POST_PATTERN_LIST_MEMBER; cd++)
#else
	while (FCGI_Accept() >= 0)
#endif
	{
#ifdef CLIDEBUGMODE
		char *requestMethod = "POST";
#else
		char *requestMethod = getenv("REQUEST_METHOD");
#endif

		// Common header for all responses
		printf("Content-type: text/html\r\n\r\n");
		if (requestMethod != NULL)
		{
			if (strcmp(requestMethod, "POST") == 0)
			{
				post_cmd_t *pc = post_cmd_parse();
				if (pc && (pc->json || (pc->username && pc->action)))
				{
					if (pc->json)
					{
						int sock = user_session_get_socket(pc->username);
						json_data_send_and_print(pc->json, sock);
					}
					// check if it is an user account management request
					else if (strstr(pc->action, "LogIn") != NULL)
					{
						handle_login(pc);
					}
					// check if it is an user log out request
					else if (strstr(pc->action, "LogOut") != NULL)
					{
						int sock = user_session_get_socket(pc->username);
						json_t *jsondata = json_pack("{s:s}", "request", "quit");
						json_data_send_and_print(jsondata, sock);
						user_session_delete(pc->username);
					}
					else if (strstr(pc->action, "getnavbarlist") != NULL)
					{
						int sock = user_session_get_socket(pc->username);
						json_t *jsondata = json_pack("{s:s}", "request", "getnavbarlist");
						json_data_send_and_print(jsondata, sock);
					}
					else if (strstr(pc->action, "loadcontent") != NULL)
					{
						if (pc->page_name)
						{
							int sock = user_session_get_socket(pc->username);
							json_t *jsondata =
								json_pack("{s:s,s:s}", "request", "loadcontent", "page_name", pc->page_name);
							json_data_send_and_print(jsondata, sock);
						}
						else
						{
							printf("{ \"error\" : \"Cannot load content if page name is not provided\" }");
						}
					}
					else
					{
						printf("Unknown POST command :%s", pc->command);
					}
					post_cmd_free(pc);
				}
				else
				{
					printf("Fail to parse post command");
				}
			}
			else
			{
				printf("Unhandled request method");
			}
		}
	}
	return 0;
}
