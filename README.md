
# Generic web interface

This software is a generic web interface to manage Linux embedded system.
It is designed and tested using lighttpd but any web server compatible with fastcgi

## Manual build and test
### Install dependencies
```
sudo apt install meson libfcgi-dev lighttpd
```
### Configure
```
meson setup build
```
### Build FCGI script
```
meson compile -C build
```
### Install files
```
sudo meson install -C build
```
### Test web app
```
sudo lighttpd -D -f /etc/lighttpd/lighttpd.conf
```
__You are now ready to test the code by entering https://127.0.0.1 in your browser__

## Using ELF
### Install ELF
If you never use ELF see https://gitlab.com/embedded_linux_factory/elf

```
mkdir ~/src/
cd ~/src
git clone https://gitlab.com/embedded_linux_factory/elf.git
```
### Build
```
~/elf/elfrun build
```
### Install
```
~/elf/elfrun deploy
```
### Test web app
```
~/elf/elfrun test
```
__You are now ready to test the code by entering https://127.0.0.1:8443 in your browser__
### Run the container interactively
```
~/elf/elfrun -i
```

## Generate your own self signed certificate
```
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/lighttpd/server.key -out /etc/lighttpd/server.crt
```

## RWS web server minimal configuration
The script must have sudo rights
```
sudo chmod u+s /var/www/html/webapp.fcgi
```

and a user group `web_interface_users` must have been created (i.e. using the `groupadd` command) to add the new users to this group
The webserver also need to have access to the `systemctl restart systemd-networkd` command without password, to do that we must add to the sudoers file (or sudoers.d) the folowwing line
```
www-data ALL=NOPASSWD: /bin/systemctl restart  systemd-networkd
```
As we already add limitations to the users who access the web server ethernet configuration, only admin users can ask for a network reboot.
