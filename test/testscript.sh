# SPDX-License-Identifier: GPL-2.0-only
#!/usr/bin/env bash

set -e

sudo lighttpd -D -f /etc/lighttpd/lighttpd.conf &

bash

exit 0